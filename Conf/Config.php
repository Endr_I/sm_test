<?php


namespace App\Conf;


use Exception;

final class Config
{
    private $array = [];

    private static $instance;

    private function __construct(array $array)
    {
        $this->array = $array;
    }

    /**
     * @return Config
     * @throws Exception
     */
    public static function getInstance(): self
    {
        if (self::$instance === null) {
            $file = __DIR__.'/config.yml';
            if (file_exists($file)) {
                $config = yaml_parse_file(__DIR__ . '/config.yml');
            } else {
                throw new Exception('Cannot read config file');
            }
            self::$instance = new static($config);
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

 public function __get($name)
 {
     return @$this->array[$name];
 }
}