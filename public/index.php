<?php

use App\Base;
use App\Log\Logger;
use App\Models\Posts;
use GuzzleHttp\Exception\RequestException;

require __DIR__."/../bootstrap.php";

try {
    $app = new Base();
    $app->render($app->getHttp()->fetch('GET', Posts::class, [
        'query' => [
            'page' => $_GET['page'] ?: 1,
        ],
    ]));
} catch (Exception $e) {
    http_response_code(500);
    try {
        Logger::error($e->getMessage(), [
            'stack' => $e->getTraceAsString(),
        ]);
    } catch (Exception $exception) {
        $e = $exception;
    }
    echo json_encode([
        'message' => $e->getMessage(),
    ]);
}
