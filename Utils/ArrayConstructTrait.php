<?php


namespace App\Utils;


trait ArrayConstructTrait
{
    public function __construct($data)
    {
        if (is_array($data)) {
            foreach ($this as $key => &$value) {
                $method = 'set'.ucwords($key, '_');
                if (method_exists($this, $method)) {
                    $this->{$method}(@$data[$key]);
                } else {
                    $value = @$data[$key];
                }
            }
        }
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}