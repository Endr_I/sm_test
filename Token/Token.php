<?php


namespace App\Token;


use App\Conf\Config;

final class Token
{
    private $value = '';
    private $timestamp = 0;
    private static $instance;

    private function __construct(array $data)
    {
        $this->value = @$data['value'] ?: '';
        $this->timestamp = @$data['timestamp'] ?: 0;
    }

    public function __toString()
    {
        return $this->valid() ? $this->value : '';
    }

    public function valid(): bool
    {
        $conf = Config::getInstance();
        return $this->value && $this->timestamp && (time() - $this->timestamp) < $conf->token_lifetime;
    }

    private static $file_name = __DIR__.'/token';
    public static function set(string $token)
    {
        $data = [
            'value' => $token,
            'timestamp' => time(),
        ];
        self::$instance = new static($data);
        file_put_contents(self::$file_name, \GuzzleHttp\json_encode($data));
    }

    public static function get(): self
    {
        if (self::$instance === null) {
            self::$instance = new static(json_decode(file_get_contents(self::$file_name), true) ?: []);
        }
        return self::$instance;
    }
}