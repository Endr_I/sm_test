<?php


namespace App\Models;


class BaseModel implements \JsonSerializable
{

    public static function getUri()
    {
        return strtolower((new \ReflectionClass(static::class))->getShortName());
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
