<?php
namespace App\Models;


use App\Utils\ArrayConstructTrait;

class ResponseMeta extends BaseModel
{
    use ArrayConstructTrait;

    private $request_id;


    /**
     * @return string
     */
    public function getRequestId(): string
    {
        return $this->request_id;
    }

    /**
     * @param string $request_id
     * @return ResponseMeta
     */
    public function setRequestId($request_id): self
    {
        $this->request_id = $request_id;
        return $this;
    }
}