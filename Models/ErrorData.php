<?php


namespace App\Models;


use App\Utils\ArrayConstructTrait;

class ErrorData extends BaseModel
{
    use ArrayConstructTrait;

    private $message;

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param mixed $error
     * @return ErrorData
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }
}