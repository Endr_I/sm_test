<?php
namespace App\Models;


use App\Utils\ArrayConstructTrait;

class ResponseData extends BaseModel
{
    use ArrayConstructTrait;

    private $meta;
    private $data;
    private $error;

    /**
     * @return ResponseMeta
     */
    public function getMeta(): ResponseMeta
    {
        return $this->meta;
    }

    /**
     * @param ResponseMeta $meta
     * @return ResponseData
     */
    public function setMeta($meta): self
    {
        $this->meta = new ResponseMeta($meta);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return ResponseData
     */
    public function setData($data): self
    {
        if ($data) {
            $this->data = $data;
        }
        return $this;
    }

    /**
     * @return ErrorData
     */
    public function getError(): ErrorData
    {
        return $this->error;
    }

    /**
     * @param array $error
     * @return ResponseData
     */
    public function setError($error): self
    {
        $this->error = new ErrorData($error);
        return $this;
    }
}