<?php
namespace App\Models;


use App\Utils\ArrayConstructTrait;
use DateTime;
use Exception;

class Post extends BaseModel
{
    use ArrayConstructTrait;

    private $id;
    private $from_name;
    private $from_id;
    private $message;
    private $type;
    private $created_time;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Post
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFromName(): string
    {
        return $this->from_name;
    }

    /**
     * @param string $from_name
     * @return Post
     */
    public function setFromName($from_name): self
    {
        $this->from_name = $from_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getFromId()
    {
        return $this->from_id;
    }

    /**
     * @param string $from_id
     * @return Post
     */
    public function setFromId($from_id): self
    {
        $this->from_id = $from_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return Post
     */
    public function setMessage($message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Post
     */
    public function setType($type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedTime(): DateTime
    {
        return $this->created_time;
    }

    /**
     * @param string $created_time
     * @return Post
     */
    public function setCreatedTime($created_time): self
    {
        try {
            $this->created_time = new DateTime(strtotime($created_time));
        } catch (Exception $e) {}
        return $this;
    }

}