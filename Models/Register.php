<?php
namespace App\Models;


use App\Utils\ArrayConstructTrait;

class Register extends BaseModel
{
    use ArrayConstructTrait;

    private $client_id;
    private $email;
    private $sl_token;

    public function getClientId(): string
    {
        return (string) $this->client_id;
    }

    public function setClientId(string $value): self
    {
        $this->client_id = $value;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $value): self
    {
        $this->email = $value;
        return $this;
    }

    public function getSlToken(): string
    {
        return $this->sl_token;
    }

    public function setSlToken(string $value): self
    {
        $this->sl_token = $value;
        return $this;
    }

    public static function getUri()
    {
        return 'register';
    }
}