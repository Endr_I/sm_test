<?php
namespace App\Models;


use App\Utils\ArrayConstructTrait;

class Posts extends BaseModel
{
    use ArrayConstructTrait;

    private $page;
    private $posts;

    public static function getUri()
    {
        return 'posts';
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     * @return Posts
     */
    public function setPage($page): self
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /**
     * @param array $posts
     * @return Posts
     */
    public function setPosts(array $posts): self
    {
        $f = function ($post) {
            return new Post($post);
        };
        $this->posts = array_map($f, $posts);
        return $this;
    }
}