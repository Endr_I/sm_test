<?php


namespace App\Log;


use App\Conf\Config;
use Monolog\Handler\StreamHandler;

class Logger
{
    private static $instance;

    /**
     * @return mixed
     */
    private static function getInstance(): \Monolog\Logger
    {
        if (self::$instance === null) {
            $debug = false;
            self::$instance = new \Monolog\Logger('log');
            try {
                $conf = Config::getInstance();
                $debug = !!$conf->debug_mode;
            } catch (\Exception $exception) {}
            if ($debug) {
                self::$instance->pushHandler(new StreamHandler(__DIR__.'/debug.log', \Monolog\Logger::DEBUG));
            }
            self::$instance->pushHandler(new StreamHandler(__DIR__.'/error.log', \Monolog\Logger::ERROR));
        }
        return self::$instance;
    }

    public static function error($message, array $context = []): void
    {
        self::getInstance()->error($message, $context);
    }

    public static function debug($message, array $context = []): void
    {
        self::getInstance()->debug($message, $context);
    }
}