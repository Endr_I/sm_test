<?php

namespace App;

use App\Conf\Config;
use App\HttpClient\GuzzleClient;
use App\Log\Logger;

class Base
{
    private $http;

    public function __construct()
    {
        $this->http = new GuzzleClient();
    }

    /**
     * @return GuzzleClient
     */
    public function getHttp(): GuzzleClient
    {
        return $this->http;
    }

    /**
     * @return Config
     * @throws \Exception
     */
    public function getConfig(): Config
    {
        return Config::getInstance();
    }

    public function render($data)
    {
        echo json_encode($data);
    }
}
