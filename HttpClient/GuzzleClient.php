<?php


namespace App\HttpClient;


use App\Conf\Config;
use App\Log\Logger;
use App\Models\BaseModel;
use App\Models\ErrorData;
use App\Models\ModelInterface;
use App\Models\Posts;
use App\Models\Register;
use App\Models\ResponseData;
use App\Token\Token;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Promise\PromiseInterface;
use Psr\Http\Message\ResponseInterface;

class GuzzleClient extends Client
{

    public function __construct(array $config = [])
    {
        $conf = Config::getInstance();
        if (!isset($config['base_uri'])) {
            $config['base_uri'] = $conf->api_url;
        }
        $this->sl_token = Token::get();
        parent::__construct($config);
    }

    public function fetch($method, string $model, $options = []): BaseModel
    {
        return $this->fetchAsync($method, $model, $options)->wait();
    }

    public function fetchAsync($method, string $model, $options = []): PromiseInterface
    {
        $uri = is_subclass_of($model, BaseModel::class) ? $model::getUri() : (new \ReflectionClass($model))->getShortName();
        return $this->requestAsync($method, $uri, $options)->then(
            function (ResponseInterface $response) use ($model) {
                $res = new ResponseData(json_decode($response->getBody()->getContents(), true));
                return new $model($res->getData());
            },
            function (Exception $exception) {
                http_response_code(500);
                if ($exception instanceof RequestException) {
                    $res = new ResponseData(json_decode($exception->getResponse()->getBody()->getContents(), true));
                    Logger::debug($exception->getMessage());
                    return $res->getError();
                }
                throw $exception;
            },
        );
    }

    public function fetchToken(): PromiseInterface {
        $conf = Config::getInstance();
        return parent::requestAsync('POST', 'register?', [
            'json' => [
                'client_id' => $conf->client_id,
                'email' => $conf->email,
                'name' => $conf->name,
            ],
        ])->then(
            function (ResponseInterface $response) {
                $res = new ResponseData(json_decode($response->getBody()->getContents(), true));
                $register = new Register($res->getData());
                Token::set($register->getSlToken());
                return $response;
            },
            function (RequestException $e) {
                throw $e;
            }
        );
    }

    public function requestAsync($method, $uri = '', array $options = [])
    {
        if (!Token::get()->valid()) {
            return $this->fetchToken()->then(
                function (ResponseInterface $response) use ($options, $uri, $method) {
                    return parent::requestAsync($method, $uri, $this->handleOptions($options, $method));
                }
            );
        }
        return parent::requestAsync($method, $uri, $this->handleOptions($options, $method));
    }

    private function handleOptions(array $options, $method): array {
        $prop = !strcasecmp($method, 'get') ? 'query' : 'json';
        $token = Token::get();
        if (isset($options[$prop])) {
            if (!isset($options[$prop]['sl_token'])) {
                $options[$prop]['sl_token'] = (string)$token;
            }
        } else {
            $options[$prop] = ['sl_token' => (string)$token];
        }
        return $options;
    }

}